package com.marcoandrade.androidchallenge.templates;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by MarcoPolo on 1/5/2018.
 */



public class IndexedCacheTest {


    IndexedCache<String> mCache;

    @Before
    public void setup(){

        mCache = new IndexedCache<>();
    }

    @Test
    public void testSet()
    {
        mCache.set(1, "Item1");
        mCache.set(2, "Item2");
        mCache.set(3, "Item3");

        // Updating the 2nd item
        mCache.set(2, "Item2Updated");

        // it should have 3 items
        assertEquals(mCache.size(), 3);
    }

    @Test
    public void testExists()
    {
        // Inserting...
        mCache.set(3, "Item3");
        mCache.set(2, "Item2");

        // Updating...
        mCache.set(3, "Item3Updated");

        assertEquals(mCache.exists(3),true);
    }

    @Test
    public void testGet()
    {
        mCache.set(1, "Item1");
        mCache.set(2, "Item2");
        mCache.set(3, "Item3");

        // Updating the 2nd item
        mCache.set(2, "Item2Updated");

        assertEquals(mCache.get(2),"Item2Updated");
    }

}
