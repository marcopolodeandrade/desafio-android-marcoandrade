package com.marcoandrade.androidchallenge.templates;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by MarcoPolo on 1/6/2018.
 */

public class DataSourceCacheTest {

    DataSourceCache<String> mCache;

    @Before
    public void setup(){
        mCache = new DataSourceCache<>();
    }

    @Test
    public void testIsEmpty(){

        assertTrue( mCache.isEmpty());

        mCache.add(Arrays.asList("1"));

        assertFalse(mCache.isEmpty());

    }


    @Test
    public void testAdd(){


        assertFalse(mCache.isAvailable(0));

        mCache.add(Arrays.asList("0"));

        assertTrue(mCache.isAvailable(0));

        mCache.add(Arrays.asList("1","2","3","4"));

        // index 4 should be available
        assertTrue(mCache.isAvailable(4));

        // but not the index 5
        assertFalse(mCache.isAvailable(5));
    }

    @Test
    public void testRemoveFromBegin(){

        mCache.add(Arrays.asList("0","1","2","3","4"));

        // removing "0","1","2"
        mCache.removeFromBegin(3);

        // indexes 0,1 and 2 should not be available anymore
        assertFalse(mCache.isAvailable(0));
        assertFalse(mCache.isAvailable(1));
        assertFalse(mCache.isAvailable(2));

        // but 3 and 4 still exist
        assertTrue(mCache.isAvailable(3));
        assertTrue(mCache.isAvailable(4));

        // Removing one more item from 'begin' should remove the index 3
        mCache.removeFromBegin(1);

        // Index 3 is no longer available
        assertFalse(mCache.isAvailable(3));
    }

    @Test
    public void testRemoveFromBegin_MoreThanPossible(){

        mCache.add(Arrays.asList("0","1"));

        assertEquals(2, mCache.getMaxQuantityAvailable());

        // removing more items than is possible should clear the list with no exceptions
        try {
            mCache.removeFromBegin(3);
        }
        catch (Exception unexpectedEx)
        {
            fail("Trying to remove more items than the available quantity should not be a real problem");
        }

        assertEquals(0, mCache.getMaxQuantityAvailable());
    }

    @Test
    public void testInsertBefore() {

        mCache.add(Arrays.asList("0","1","2","3","4"));

        // removing "0","1","2"
        mCache.removeFromBegin(3);

        // indexes 0,1 and 2 should not be available any more
        assertFalse(mCache.isAvailable(0));
        assertFalse(mCache.isAvailable(1));
        assertFalse(mCache.isAvailable(2));

        // Inserting "0","1","2" again before "3","4"
        mCache.insertBefore(Arrays.asList("0","1","2"));

        // The Indexes 0,1,2 should be available again
        assertTrue(mCache.isAvailable(0));
        assertTrue(mCache.isAvailable(1));
        assertTrue(mCache.isAvailable(2));
    }

    @Test
    public void testInsertBeforeMistake(){

        mCache.add(Arrays.asList("ForIndex0","ForIndex1"));

        boolean inserted = mCache.insertBefore(Arrays.asList("BeforeIndex0", "BeforeIndex0"));

        assertFalse("It should not be possible to insert items with index < 0", inserted);

        // Qty available must be only 2, and not 4 items
        assertEquals(mCache.getMaxQuantityAvailable(),2);
    }

    @Test
    public void testRemoveFromEnd(){

        mCache.add(Arrays.asList("0","1","2","3","4"));

        // removing the items "3","4"
        mCache.removeFromEnd(2);

        // indexes 3,4 should not be available anymore
        assertFalse("Index 3 should not be available", mCache.isAvailable(3));
        assertFalse("Index 4 should not be available", mCache.isAvailable(4));

        assertEquals("3 items left expected", 3, mCache.getMaxQuantityAvailable());

        // Inserting "4","5" again
        mCache.add(Arrays.asList("4","5"));

        // The Indexes 3,4 should be available again
        assertTrue(mCache.isAvailable(3));
        assertTrue(mCache.isAvailable(4));

        // but not index 5
        assertFalse(mCache.isAvailable(5));
    }

    @Test
    public void testRemoveFromEnd_MoreThanPossible(){

        mCache.add(Arrays.asList("0","1"));

        assertEquals(2, mCache.getMaxQuantityAvailable());

        // removing more items than is possible should clear the list with no exceptions
        try {
            mCache.removeFromBegin(4);
        }
        catch (Exception unexpectedEx)
        {
            fail("Trying to remove more items than the available quantity should not be a real problem");
        }

        assertEquals(0, mCache.getMaxQuantityAvailable());
    }

    @Test
    public void testGet(){

        mCache.add(Arrays.asList("0","1","2","3","4","5","6","7","8","9"));

        assertEquals("3", mCache.get(3));

        // after removing "0","1", the item "3" should be available for the same index
        mCache.removeFromBegin(2);
        assertEquals("3", mCache.get(3));


        // after removing "7","8","9" the item "6" should be still available in the index 6
        mCache.removeFromBegin(3);
        assertEquals("6", mCache.get(6));

    }

    @Test
    public void testGetNotExisting(){

        mCache.add(Arrays.asList("0","1","2"));

        assertEquals("An exception with specific message is expected in this case",
                null, mCache.get(3));

    }

}
