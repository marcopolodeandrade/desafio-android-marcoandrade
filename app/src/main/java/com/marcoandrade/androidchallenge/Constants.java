package com.marcoandrade.androidchallenge;

/**
 * Created by MarcoPolo on 12/5/2017.
 */

public final class Constants {
    public static final String PARAM_REPOSITORY_NAME = "repository.name";
    public static final String PARAM_OWNER_LOGIN = "owner.login";

}
