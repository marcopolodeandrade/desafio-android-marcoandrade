package com.marcoandrade.androidchallenge.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marcoandrade.androidchallenge.repositories.models.OwnerInfo;

/**
 * Created by MarcoPolo on 1/7/2018.
 *
 * Template for common json parsing in this project
 */

public class JsonParser<TResult> {

    private final Class<TResult> mResultClass;

    public JsonParser(Class<TResult> resultClass){
        mResultClass = resultClass;
    }

    public TResult Parse(String jsonString){

        TResult result=null;

        try {
            ObjectMapper objectMapper = new ObjectMapper();

            result = objectMapper.readValue(jsonString, mResultClass);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return result;
    }
}
