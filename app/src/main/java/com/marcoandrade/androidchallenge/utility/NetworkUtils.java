package com.marcoandrade.androidchallenge.utility;

/**
 * Created by MarcoPolo on 12/2/2017.
 */


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Scanner;

public final class NetworkUtils {

    private static final String TAG = NetworkUtils.class.getSimpleName();

    /** Parametro com a chave de pesquisa*/
    final static String PARAM_QUERY = "q";

    /** Parametro para a opção de ordenacao */
    final static String PARAM_SORT = "sort";

    /** parametro da paginacao */
    final static String PARAM_PAGE = "page";

    public static boolean isNetworkConnected(Context context) {
        try {
            final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
            return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
        }
        catch (Exception e){
            Log.d(TAG, e.getMessage());
            return false;
        }
    }

    public static boolean isInternetAvailable() {
        try {

            final InetAddress address = InetAddress.getByName("www.google.com");
            return !address.equals("");

        } catch (UnknownHostException e) {
            Log.d(TAG, "UNKNOWN HOST - WHEN verifying the internet connection :" + e.getMessage());
            return false;
        }
        catch (Exception e){
            Log.d(TAG, e.getMessage());
            return false;
        }
    }

    /**
     * Builds the URL used to query GitHub repositories
     *
     * @param searchQuery The keyword that will be queried for.
     * @param pageNumber The number of the page requested
     * @return The URL to use to query the GitHub.
     */
    public static URL buildUrlGetGitHubRepositories(String searchQuery, int pageNumber) {

        final String GITHUB_BASE_URL = "https://api.github.com/search/repositories";
        final String sortBy = "stars";

        Uri builtUri = Uri.parse(GITHUB_BASE_URL).buildUpon()
                .appendQueryParameter(PARAM_QUERY, searchQuery)
                .appendQueryParameter(PARAM_SORT, sortBy)
                .appendQueryParameter(PARAM_PAGE, String.valueOf(pageNumber))
                .build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }

    /**
     * Builds the URL used to query a GitHub Repository Pull Requests
     *
     * @param ownerName The owner's name
     * @param repositoryName The repository's name
     * @param pageNumber The number of the page requested
     * @return The URL to use to query the GitHub.
     */
    public static URL buildUrlGetGitHubRepositoryPullRequests(String ownerName, String repositoryName, int pageNumber) {

        // Example: https://api.github.com/repos/OnwerName/RepositoryName/pulls

        final String GITHUB_BASE_URL = "https://api.github.com/repos";
        final String SEGMENT_PULLS = "pulls";

        //String GITHUB_BASE_URL = "https://api.github.com/repos/" + ownerName + "/" + repositoryName + "/pulls";

        Uri builtUri = Uri.parse(GITHUB_BASE_URL).buildUpon()
                .appendPath(ownerName)
                .appendPath(repositoryName)
                .appendPath(SEGMENT_PULLS)
                .appendQueryParameter(PARAM_PAGE, String.valueOf(pageNumber))
                .build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }


    /**
     * This method returns the entire result from the HTTP response.
     *
     * @param url The URL to fetch the HTTP response from.
     * @return The contents of the HTTP response.
     * @throws IOException Related to network and stream reading
     */
    public static String getResponseFromHttpUrl(URL url) throws IOException {

        Log.d(TAG, "Requesting: " + url.toString());

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            String returnValue = null;

            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();

            if (hasInput) {
                returnValue = scanner.next();
            }
            in.close();

            return returnValue;

        } finally {
            urlConnection.disconnect();
        }
    }


    public static byte[] getBytesContentFromResponseHttp(URL url) throws IOException {

        Log.d(TAG, "Requesting: " + url.toString());

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            String returnValue = null;

            InputStream in = urlConnection.getInputStream();

            byte[] byteArray = readBytes(in);

            in.close();

            Log.d(TAG, "Content Size (bytes[]) : " + String.valueOf(byteArray.length) + " bytes");

            return byteArray;
        }
        finally {
            urlConnection.disconnect();
        }
    }

    static byte[] readBytes(InputStream inputStream) throws IOException {
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }

}
