package com.marcoandrade.androidchallenge.utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by MarcoPolo on 12/8/2017.
 */

public final class BitmapUtils {

    public static Bitmap getBitmapFrom(byte[] byteArray){

        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        return  bitmap;
    }

}
