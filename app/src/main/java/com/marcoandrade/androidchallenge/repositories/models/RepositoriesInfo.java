package com.marcoandrade.androidchallenge.repositories.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Resultado da consulta aos Repositórios do GitHub
 * Contem:
 *      Lista de repositórios com as principais informações de cada Owner
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class RepositoriesInfo {

    /**
     * About the Owner
     */
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Owner{

        public Owner(){/*used for parsing*/}

        @JsonProperty("id")
        public int id;

        @JsonProperty("login")
        public String login;

        @JsonProperty("type")
        public String type;

        // Link to owner's complete information
        @JsonProperty("url")
        public String url;

        // Link to owner's avatar/picture
        @JsonProperty("avatar_url")
        public String avatarUrl;
    }

    /**
     * About the Repository
     */
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class RepositoryInfo {

        public RepositoryInfo(){/*used for parsing*/}

        @JsonProperty("id")
        public int id;

        @JsonProperty("name")
        public String name;

        @JsonProperty("description")
        public String description;

        @JsonProperty("owner")
        public Owner owner;

        @JsonProperty("stargazers_count")
        public int stargazersCount;

        @JsonProperty("forks")
        public int forksCount;
    }


    public RepositoriesInfo(){/*used for parsing*/}


    /**
     * Quantidade de respositórios disponíveis para consulta no GitHub
     */
    @JsonProperty("total_count")
    public int totalCount;

    /**
     * Repositórios obtidos nesta consulta/pagina
     */
    @JsonProperty("items")
    public List<RepositoryInfo> items;

}
