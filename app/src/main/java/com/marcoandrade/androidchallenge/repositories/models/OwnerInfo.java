package com.marcoandrade.androidchallenge.repositories.models;

import android.graphics.Bitmap;


//import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Informações do Owner do 'Repositório' carregadas posteriormente
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OwnerInfo {

    // Id do owner
    public int id;

    // Owner's Full Name
    public String name;

    // Owner's Picture
    public Bitmap avatarBitmap;

    public OwnerInfo(){/*Utilizado pelo json parser*/}


}
