package com.marcoandrade.androidchallenge.repositories;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.marcoandrade.androidchallenge.R;
import com.marcoandrade.androidchallenge.repositories.models.OwnerInfo;
import com.marcoandrade.androidchallenge.repositories.models.RepositoriesInfo;
import com.marcoandrade.androidchallenge.templates.DataSourceCache;
import com.marcoandrade.androidchallenge.templates.IndexedCache;
import com.marcoandrade.androidchallenge.utility.BitmapUtils;
import com.marcoandrade.androidchallenge.utility.JsonParser;
import com.marcoandrade.androidchallenge.utility.NetworkUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * The Adapter for Repositories RecyclerView
 */
public class ViewAdapter extends RecyclerView.Adapter<ViewAdapter.RepositoryViewHolder> {

    private static final String TAG = ViewAdapter.class.getSimpleName();

    /**
     * Quantidade maxima de paginas de dados que serão mantidas no datasource
     * */
    private static final int MAX_NUMBER_OF_PAGES_KEPT_IN_MEMORY = 4;

    /**
     * A quantidade de itens que poderá aparecer na parte inferior da lista antes do DataSource carregar as nova pagina de items
     * */
    private static final int EXTRA_ITEMS_AVAILABLE_WHILE_LOADING_ASYNC = 2;

    /**
     * Intervalo (ms) entre cada request extra feito para obter dados complementares dos usuarios
     *
     * For unauthenticated requests, the rate limit allows you to make up to 10 requests per minute.
     */
    private static final int MIN_INTERVAL_TO_EXTRA_REQUESTS = 500;

    // Usado nos testes - False desabilita os requests do Nome e Avatar do Owner (economizando requests)
    private static final boolean ENABLE_SUB_REQUESTS = true;

    /**
     * The interface that receives onClick messages.
     */
    public interface IOnItemClickListener {

        /**
         * Ocorre quando o usuario 'toca' em um repositório
         * @param clickedItemIndex Indice clicado
         * @param repositoryInfo  Dados relacionados ao item clicado
         */
        void onRepositoryItemClick(int clickedItemIndex, RepositoriesInfo.RepositoryInfo repositoryInfo);
    }

    /*
     * The number of ViewHolders that have been created.
     */
    private static int viewHolderCount;

    /*
     * Listener para os eventos
     */
    protected IOnItemClickListener mOnItemClickListener = null;


    /**
     * Quantidade de itens disponíveis para o RecyclerView
     *     Varia de acordo com a quantidade de itens no DataSource
     *     Usado também para limitar o scrolling ascendente do recyclerview
     * */
    private int mNumberOfItemsAvaiable;

    // A posição solicitada anteriormente no onBindViewHolder
    private int mPreviousPositionRequired = 0;

    // Items disponíveis para a View
    DataSourceCache<RepositoriesInfo.RepositoryInfo> mDataSourceCache = new DataSourceCache<>();

    // Informações de Owners mantidas em memória (passou a ser static para economizar requests em ambiente de testes)
    static IndexedCache<OwnerInfo> ownerInfoCache = new IndexedCache<OwnerInfo>();

    // Itens de visualização que aguardam o loading assíncrono para serem preenchidos
    List<PendingBind> pendingBindList = new ArrayList<>();

    // TODO: Melhoria: A lista abaixo é na prática utilizada como Fila,  portanto, trocar por uma implementação de fila
    // #GetOwner - Lista de Owners que precisam ser carregados para a view
    ArrayList<PendingGetOwnerInfo> pendingGetOwnerInfoList = new ArrayList<>();


    // Valores para controle de paginacao

    // Ultima pagina carregada para o DataSource
    int lastPageNumberLoaded = 0;

    // Valores da 'primeira' pagina carregada no datasource (numero e quantidade de itens)
    int currentDataSourceFirstPageNumber = 0;
    int currentDataSourceFirstPageCount = 0;

    // Valores da 'ultima' pagina carregada no datasource (numero e quantidade de itens)
    int currentDataSourceLastPageNumber = 0;
    int currentDataSourceLastPageCount = 0;


    /** true indica que o RecyclerView está solicitando items de posicao anterior (usuario rolando a lista para o topo)*/
    boolean isScrollingToBegin = false;


    // Sinaliza que uma consulta está em andamento , carregando um novo bloco de dados para o mDataSourceFake
    // A variavel é atualizada somente pela Thread da UI
    boolean isLoadingAsync = false;

    public boolean isLoadingDataAsync(){return isLoadingAsync;}

    /**
     * Constructor
     * @param numberOfItems Numero de itens inicial (aparente)
     *                      - O valor permite que o RecyclerView solicite ao Adapter no maximo esta quantidade inicial de itens,
     *                      o que por sua vez dispara o load async da primeira pagina de daodos
     *                      (O valor precisa ser maior que zero)
     */
    public ViewAdapter(int numberOfItems) {
        mNumberOfItemsAvaiable = numberOfItems;
        viewHolderCount = 0;
    }

    public void setOnItemClickListener(IOnItemClickListener listener){
        this.mOnItemClickListener = listener;
    }

    /**
     * This method simply returns the number of items to display. It is used behind the scenes
     * to help layout our Views and for animations.
     *
     * @return The number of items available in our forecast
     */
    @Override
    public int getItemCount() {return mNumberOfItemsAvaiable;}

    /** Quando executada, verifica as últimas paginas consultadas e elimina do cache itens desnecessarios para a consulta atual do RecyclerView
     *  Utilizado para retirar da memória dados que 'ficaram pra trás' no scrolling, e que talvez não sejam mais utilizados */
    private void cleaningCache()
    {
        if(currentDataSourceLastPageNumber - lastPageNumberLoaded >= MAX_NUMBER_OF_PAGES_KEPT_IN_MEMORY)
        {
            // Remove a ultima 'pagina' do cache
            mDataSourceCache.removeFromEnd(currentDataSourceLastPageCount);

            // Atualiza o contador..
            currentDataSourceLastPageNumber -= 1;
        }
        else if(lastPageNumberLoaded - currentDataSourceFirstPageNumber >= MAX_NUMBER_OF_PAGES_KEPT_IN_MEMORY)
        {
            // Remove o primeiro 'bloco' do cache
            mDataSourceCache.removeFromBegin(currentDataSourceFirstPageCount);

            // Atualiza o contador
            currentDataSourceFirstPageNumber += 1;
        }
    }

    /**
     * This gets called when each new ViewHolder is created. This happens when the RecyclerView
     * is laid out. Enough ViewHolders will be created to fill the screen and allow for scrolling.
     *
     * @param viewGroup The ViewGroup that these ViewHolders are contained within.
     * @param viewType  If your RecyclerView has more than one type of item (which ours doesn't) you
     *                  can use this viewType integer to provide a different layout. See
     *                  {@link android.support.v7.widget.RecyclerView.Adapter#getItemViewType(int)}
     *                  for more details.
     * @return A new NumberViewHolder that holds the View for each list item
     */
    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.repository_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        RepositoryViewHolder viewHolder = new RepositoryViewHolder(view);

        viewHolder.clearLoadingMessage();
        viewHolder.empty();

        viewHolderCount++;

        Log.d(TAG, "onCreateViewHolder: ViewHolders created (" + String.valueOf(viewHolderCount) + ")");

        return viewHolder;
    }


    /** true indica que:
     *      o Adapter sugere que o scroll seja interrompido pois os dados solicitados ainda estão sendo carregados
     */
    public boolean shouldHoldOnTheScroll(){

        // REGRA: Para sugerir que o scroll seja interrompido
        //     Se o usuario está subindo a lista e já temos mais de 2 Views aguardando pelo bind
        return (isScrollingToBegin && pendingBindList.size()>=EXTRA_ITEMS_AVAILABLE_WHILE_LOADING_ASYNC);
    }

    /**
     * OnBindViewHolder is called by the RecyclerView to display the data at the specified
     * position. In this method, we update the contents of the ViewHolder to display the correct
     * indices in the list for this particular position, using the "position" argument that is conveniently
     * passed into us.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(RepositoryViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: " + "# " + position);

        // Se deseja carregar um item indisponivel no cache
        if(!mDataSourceCache.isAvailable(position)){

            // Se pela 'position' solicitada  concluí-se que o RecyclerView está voltando para o topo...
            isScrollingToBegin = (position < mPreviousPositionRequired);

            Log.d("onBindViewHolder", "Data Unavailable: Position " + String.valueOf(position) + ". Saving ViewHolder for later fill");

            if (pendingBindList.size() == 0) {
                holder.showLoadingMessage(); // O primeiro item aguardando a carga exibe o 'loading...'
            }

            holder.empty(); // apaga os dados da entidade

            // Guarda a referencia do viewHolder para preenchimento posterior
            pendingBindList.add(new PendingBind(holder, position));

            if(!isLoadingAsync) {
                isLoadingAsync = true;

                // Calcula o numero da pagina que está sendo necessario nesta consulta
                int pageNumberNeeded = mDataSourceCache.isEmpty() ? 1 :
                        (position > mDataSourceCache.getMaxIndexAvailable()) ? currentDataSourceLastPageNumber + 1 : currentDataSourceFirstPageNumber - 1;

                Log.d("onBindViewHolder", "Requesting Page (for GitHub):  " + String.valueOf(pageNumberNeeded));
                getRepositoriesFromGitHubAsync(pageNumberNeeded);

            }

        }
        else{

            RepositoriesInfo.RepositoryInfo itemInfo = mDataSourceCache.get(position);

            // O item esta disponivel no dataSource. Basta exibir
            holder.clearLoadingMessage();
            holder.bind(itemInfo);

            // Se já possui os dados complementares do owner.. basta exibir
            if(ownerInfoCache.exists(itemInfo.owner.id)) {
                OwnerInfo ownerInfo = ownerInfoCache.get(itemInfo.owner.id);
                holder.bind(ownerInfo);
            }
            else {
                // Incluir o viewHolder na lista para receber os dados complementares do usuario
                pendingGetOwnerInfoList.add(new PendingGetOwnerInfo(holder));
            }

            checkNextPendingOwnerInfo();
        }

        // Guarda a ultima posicao solicitada
        mPreviousPositionRequired = position;

    }


    class RepositoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        // References to last DataObjects used in bind operation for this ViewHolder
        RepositoriesInfo.RepositoryInfo mRepositoryInfo = null;

        TextView loadingTextView;

        // Dados da entidade
        TextView repoNameView;
        TextView repoDescriptionView;
        TextView repoBranchesCountView;
        TextView repoStarsCountView;
        TextView repoOwnerLoginView;
        TextView repoOwnerNameView;
        ImageView repoOwnerAvatarView;

        // Elementos usados como containers
        FrameLayout repoIndicatorsContainer;

        public RepositoryViewHolder(View itemView) {
            super(itemView);

            // Getting view objects
            loadingTextView = (TextView) itemView.findViewById(R.id.repo_loading_text);
            repoNameView         = (TextView) itemView.findViewById(R.id.repo_name);
            repoDescriptionView  = (TextView) itemView.findViewById(R.id.repo_description);
            repoBranchesCountView= (TextView) itemView.findViewById(R.id.repo_branches_count);
            repoStarsCountView   = (TextView) itemView.findViewById(R.id.repo_stars_count);
            repoOwnerLoginView   = (TextView) itemView.findViewById(R.id.repo_ownerlogin);
            repoOwnerNameView    = (TextView) itemView.findViewById(R.id.repo_ownername);
            repoOwnerAvatarView  = (ImageView) itemView.findViewById(R.id.repo_owner_avatar);

            repoIndicatorsContainer = (FrameLayout) itemView.findViewById(R.id.repo_indicators_container);

            itemView.setOnClickListener(this);
        }

        void showLoadingMessage() {

            loadingTextView.setText("Loading...");
        }

        void clearLoadingMessage(){
            loadingTextView.setText("");
        }

        void bind(RepositoriesInfo.RepositoryInfo itemData) {

            // Saving the data object reference used, for future operations
            this.mRepositoryInfo = itemData;

            repoNameView         .setText(itemData.name);
            repoDescriptionView  .setText(itemData.description);
            repoBranchesCountView.setText(String.valueOf(itemData.forksCount));
            repoStarsCountView   .setText(String.valueOf(itemData.stargazersCount));
            repoOwnerLoginView   .setText(itemData.owner.login);

            repoIndicatorsContainer.setVisibility(View.VISIBLE);

            repoOwnerNameView    .setText("");

            repoOwnerAvatarView.setImageResource(R.mipmap.ic_launcher);
            repoOwnerAvatarView.setVisibility(View.VISIBLE);
        }

        void bind(OwnerInfo ownerInfo){

            // Owner's Full Name
            repoOwnerNameView.setText((ownerInfo.name != null)? ownerInfo.name: "");

            // Owner's picture (avatar)
            if(ownerInfo.avatarBitmap != null) {
                repoOwnerAvatarView.setImageBitmap(ownerInfo.avatarBitmap);
                repoOwnerAvatarView.setVisibility(View.VISIBLE);
            }
            else {
                repoOwnerAvatarView.setImageResource(R.mipmap.ic_launcher);
                repoOwnerAvatarView.setVisibility(View.VISIBLE);
            }
        }

        void empty() {

            // forgets the data object used once
            this.mRepositoryInfo = null;

            repoIndicatorsContainer.setVisibility(View.INVISIBLE);
            repoOwnerAvatarView.setVisibility(View.INVISIBLE);

            repoNameView         .setText("");
            repoDescriptionView  .setText("");
            repoBranchesCountView.setText("");
            repoStarsCountView   .setText("");
            repoOwnerLoginView   .setText("");
            repoOwnerNameView    .setText("");
            repoOwnerNameView    .setText("");

        }

        /**
         * Called whenever a user clicks on an item in the list.
         * @param v The View that was clicked
         */
        @Override
        public void onClick(View v) {

            // Se a view está sem a entidade relacionada... apenas ignora o click
            if(null != mRepositoryInfo)
            {
                if(null != mOnItemClickListener) {
                    int clickedPosition = getAdapterPosition();

                    // O evento informa a posicao do item e os dados do item relacionado
                    mOnItemClickListener.onRepositoryItemClick(clickedPosition, mRepositoryInfo);
                }
            }
        }
    }

    class PendingBind{

        public int position;
        public RepositoryViewHolder viewHolder;

        public PendingBind(RepositoryViewHolder pViewHolder, int pPosition){
            viewHolder = pViewHolder;
            position = pPosition;
        }
    }


    class PendingGetOwnerInfo
    {
        // Referencia para os dados do Repositorio relacionado ao ViewModel no momento em que a 'pendencia' foi gerada
        public final RepositoriesInfo.RepositoryInfo repositoryInfo;

        // Referencia para o View holder que exibirá os dados do Owner
        public final RepositoryViewHolder viewHolder;

        // Carregado posteriormente
        public OwnerInfo ownerInfoResult;

        public PendingGetOwnerInfo(RepositoryViewHolder pViewHolder){

            // Guarda a referencia para os dados do repositorio
            repositoryInfo = pViewHolder.mRepositoryInfo;

            // Guarda a referencia para a viewHolder
            this.viewHolder = pViewHolder;
        }


        // Verifica se o viewHolder, do momento que a pendencia foi criada até agora, não mudou de Repositório
        //           (o viewHolder naturalmente muda de posição na View e passa a ser usado para outro repositorio e owner)
        public boolean checkRepositoryDoesNotChangedToViewHolder()
        {
            return (viewHolder.mRepositoryInfo != null && repositoryInfo.id == viewHolder.mRepositoryInfo.id);
        }
    }



    void bindPendingViewHolderList()
    {
        Log.d(TAG, "Binding pending views ... (" + pendingBindList.size() + ") waiting");

        for (PendingBind pendingBind: pendingBindList){

            RepositoryViewHolder viewHolder = pendingBind.viewHolder;

            viewHolder.clearLoadingMessage();

            if( mDataSourceCache.isAvailable(pendingBind.position))
            {
                RepositoriesInfo.RepositoryInfo itemInfo = mDataSourceCache.get(pendingBind.position);
                viewHolder.bind(itemInfo);

                // Se já possui os dados complementares do owner.. .
                if(ownerInfoCache.exists(itemInfo.owner.id)){
                    OwnerInfo ownerInfo = ownerInfoCache.get(itemInfo.owner.id);
                    viewHolder.bind(ownerInfo); // exibe
                }
                else {
                    // Coloca este view holder para receber o restante dos dados do owner
                    pendingGetOwnerInfoList.add(new PendingGetOwnerInfo(viewHolder));
                }
            }
            else{
                // Com um efetivo controle do Scroll, esta exceção não deve ocorrer

                String message ="***** bindPendingViewHolderList : pendingBind.position (" + String.valueOf(pendingBind.position) + ") indisponivel no mDataSourceCache";
                Log.d(TAG,  message);

                // Deixa em branco
                viewHolder.empty();
            }
        }

        // Ao encerrar, limpa a lista de Bindings pendentes
        pendingBindList.clear();

        checkNextPendingOwnerInfo();
    }

    void processResult(int pageNumber, RepositoriesInfo result)
    {
        if(result.items.size()>0) {

            Log.d(TAG, "processResult: " + result.items.size() + " items was received from GitHub");

            if (currentDataSourceFirstPageNumber == 0) { // First Load

                currentDataSourceFirstPageNumber = pageNumber;
                currentDataSourceFirstPageCount = result.items.size();

                currentDataSourceLastPageNumber = pageNumber;
                currentDataSourceLastPageCount = result.items.size();

                // Adiciona os items no cache
                mDataSourceCache.add(result.items);
            } else if (pageNumber > currentDataSourceLastPageNumber) {
                // Se carregou uma página posterior a ultima carregada

                // Guarda o numero da pagina e a quantidade carregada
                currentDataSourceLastPageNumber = pageNumber;
                currentDataSourceLastPageCount = result.items.size();

                // Adiciona os items ao final do Cache
                mDataSourceCache.add(result.items);
            } else if (pageNumber < currentDataSourceFirstPageNumber) {
                // Se carregou uma pagina anterior a primeira que esta em cache

                currentDataSourceFirstPageNumber = pageNumber;
                currentDataSourceFirstPageCount = result.items.size();

                // Adiciona os items no inicio da lista em cache
                mDataSourceCache.insertBefore(result.items);
            }

            // Quarda o numero da ultima pagina carregada
            lastPageNumberLoaded = pageNumber;
        }
        else
        {
            Log.d(TAG, "processResult: EMPTY RESULT was received from GitHub");
        }

        // Preenche views que aguardavam este loading (se existirem na lista de espera)...
        bindPendingViewHolderList();

        // Retira da memória itens desnecessários para a posição atual da visualização
        cleaningCache();

        // Atualiza a quantidade de items (aparente) disponiveis para o RecyclerView
        //             Obs.: O Adapter informa a quantidade aparente para limitar quantos items o RecyclerView
        //                   na parte inferior da lista , enquanto os dados da nova pagina ainda são carregados
        mNumberOfItemsAvaiable = mDataSourceCache.getMaxQuantityAvailable()+ EXTRA_ITEMS_AVAILABLE_WHILE_LOADING_ASYNC;
    }


    /**
     * Inicializa o AsyncTask que consulta o GitHub e carrega mais dados para o DataSource
     */
    private void getRepositoriesFromGitHubAsync(int pageNumber) {

        String searchQuery = "language:Java";

        URL githubSearchUrl = NetworkUtils.buildUrlGetGitHubRepositories(searchQuery, pageNumber);

        LoadRepositoriesPageTask queryTask = new LoadRepositoriesPageTask(pageNumber);

        queryTask.execute(githubSearchUrl);
    }

    /**
     * AsyncTask responsável por consultar uma pagina do GitHub e atualizar o DataSource
     */
    class LoadRepositoriesPageTask extends AsyncTask<URL, Void, RepositoriesInfo> {

        private final String TAG = LoadRepositoriesPageTask.class.getSimpleName();

        int mPageNumber;

        public LoadRepositoriesPageTask(int pageNumber){
            mPageNumber = pageNumber;
        }

        @Override
        protected void onPreExecute() {
            // This runs on UI Thread so .. we can update some view here
            super.onPreExecute();
        }

        @Override
        protected RepositoriesInfo doInBackground(URL... urls) {

            RepositoriesInfo result = null;

            try {

                URL searchUrl = urls[0];

                String searchResults = NetworkUtils.getResponseFromHttpUrl(searchUrl);

                if(null != searchResults) {
                    // Parsing...
                    result = (new JsonParser<>(RepositoriesInfo.class)).Parse(searchResults);
                }

            } catch (IOException e) {
                Log.d(TAG, "Falha ao obter dados do gitHub. Erro abaixo:");
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(RepositoriesInfo result) {

            // Executed by UI Thread

            //super.onPostExecute(s);
            try {

                if (result != null) {

                    processResult(this.mPageNumber, result);
                }
                else{
                    Log.d(TAG, "***** onPostExecute() recebeu parametro nullo");
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }

            isLoadingAsync = false;
        }

    }


    GithubLoadOwnersTask loadOwnerInfoTask = null;

    Handler startloadOwnerTaskHandler = new Handler();


    PendingGetOwnerInfo findNextPendingOwnerInfoValid()
    {
        PendingGetOwnerInfo found = null;

        while(null == found && pendingGetOwnerInfoList.size()>0) {

            // Recupera e remove o primeiro item da lista
            PendingGetOwnerInfo pendingItem = pendingGetOwnerInfoList.get(0);
            pendingGetOwnerInfoList.remove(0);

            // Se a view ainda está para o  mesmo Repositorio
            if(pendingItem.checkRepositoryDoesNotChangedToViewHolder()) {
                found = pendingItem;
            }

        }

        return found;

    }

    /**
     * Inicializa o AsyncTask que consulta o GitHub e carrega informações dos Owners de cada repositorio
     *
     *   Deve ser executada na Thread da UI
     */
    private void checkNextPendingOwnerInfo() {

        // Se já tem task em andamento , apenas ignora
        if(null == loadOwnerInfoTask)
        {
            // Pega o proximo item pendente valido
            PendingGetOwnerInfo pendingGetOwnerInfo = findNextPendingOwnerInfoValid();

            if(null != pendingGetOwnerInfo) {

                // Obtem o Id do Owner deste Repositorio
                int ownerId = pendingGetOwnerInfo.repositoryInfo.owner.id;

                // Se as informações deste usuario ja estao no cache
                if(ownerInfoCache.exists(ownerId)){
                    // Basta exibir
                    OwnerInfo userInfo = ownerInfoCache.get(ownerId);
                    pendingGetOwnerInfo.viewHolder.bind(userInfo);
                }
                else {
                    loadOwnerInfoTask = new GithubLoadOwnersTask(pendingGetOwnerInfo);

                    startloadOwnerTaskHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadOwnerInfoTask.execute();
                        }
                    }, MIN_INTERVAL_TO_EXTRA_REQUESTS);
                }
            }
        }
    }

    /**
     * AsyncTask responsável por consultar DADOS de Owners que precisam ser exibidos (mas nao constam na consulta principal)
     */
    public class GithubLoadOwnersTask extends AsyncTask<Void, Void, PendingGetOwnerInfo> {

        private final String TAG = GithubLoadOwnersTask.class.getSimpleName();

        PendingGetOwnerInfo mPendingGetOwnerInfo;

        public GithubLoadOwnersTask(PendingGetOwnerInfo pendingGetOwnerInfo){
            mPendingGetOwnerInfo = pendingGetOwnerInfo;
        }

        @Override
        protected void onPreExecute() {
            // This runs on UI Thread so .. we can update some view here
            super.onPreExecute();
        }

        @Override
        protected PendingGetOwnerInfo doInBackground(Void... params) {

            PendingGetOwnerInfo pendingGet = mPendingGetOwnerInfo;

            try {

                if(pendingGet.checkRepositoryDoesNotChangedToViewHolder())
                {
                    try {

                        if(ENABLE_SUB_REQUESTS) {
                            // Obtem os dados do Owner para obter o 'name' (nome completo) diferente do 'login'
                            URL getOwnerInfoUrl = new URL(pendingGet.repositoryInfo.owner.url);
                            String resultDataString = NetworkUtils.getResponseFromHttpUrl(getOwnerInfoUrl);

                            // Instancia o ownerInfo e preenche com o nome
                            pendingGet.ownerInfoResult = (new JsonParser<>(OwnerInfo.class)).Parse(resultDataString);

                            // Obtem o Avatar (imagem do owner)
                            URL getAvatarUrl = new URL(pendingGet.repositoryInfo.owner.avatarUrl);
                            byte[] avatarByteArray = NetworkUtils.getBytesContentFromResponseHttp(getAvatarUrl);
                            pendingGet.ownerInfoResult.avatarBitmap = BitmapUtils.getBitmapFrom(avatarByteArray);
                        }

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                }

            } catch (IOException e) {

                Log.d(TAG, "Falha ao obter dos dados do GitHub a respeito do Owner (name e avatar)");

                e.printStackTrace();
            }

            return pendingGet;
        }

        @Override
        protected void onPostExecute(PendingGetOwnerInfo result) {

            // Executed by UI Thread

            //super.onPostExecute(s);
            try {
                processDelayedResultOwnerInfo(result);
            }
            catch (Exception e){
                e.printStackTrace();
            }


        }
    }

    void processDelayedResultOwnerInfo(PendingGetOwnerInfo result){

        try {
            if (null != result && null != result.ownerInfoResult && result.checkRepositoryDoesNotChangedToViewHolder()) {

                OwnerInfo ownerInfoResult = result.ownerInfoResult;

                // Guarda em cache
                ownerInfoCache.set(ownerInfoResult.id, ownerInfoResult);

                // Mostra na view estes dados do Owner
                result.viewHolder.bind(ownerInfoResult);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally {

            // A referencia para a Task é anulada... para que uma nova task deste tipo possa ser criada..
            loadOwnerInfoTask = null;
        }

        // Verifica se tem mais viewHolders precisando complementar os dados do owner
        checkNextPendingOwnerInfo();
    }
}
