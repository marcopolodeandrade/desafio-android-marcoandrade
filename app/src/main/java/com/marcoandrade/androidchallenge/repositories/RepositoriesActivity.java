package com.marcoandrade.androidchallenge.repositories;

import android.content.Context;
import android.content.Intent;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.marcoandrade.androidchallenge.Constants;
import com.marcoandrade.androidchallenge.pullrequests.PullRequestsActivity;
import com.marcoandrade.androidchallenge.R;
import com.marcoandrade.androidchallenge.repositories.models.RepositoriesInfo;
import com.marcoandrade.androidchallenge.utility.NetworkUtils;

public class RepositoriesActivity extends AppCompatActivity implements ViewAdapter.IOnItemClickListener
{
    private static final String TAG = RepositoriesActivity.class.getSimpleName();
    private static final int SUPPOSED_INITIAL_NUMBER_OF_ITEMS = 5;

    class RecyclerViewScrollListener extends RecyclerView.OnScrollListener
    {
        final String TAG = RecyclerViewScrollListener.class.getSimpleName();

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            super.onScrolled(recyclerView, dx, dy);


            if(mAdapter.shouldHoldOnTheScroll()){

                // Uma nova pagina de itens está sendo carregada em background
                // E o scrolling precisa ser interrompido até a carga completa destes dados

                Log.d(TAG, "Stopping the scroll: Waiting for dataSource be updated...");
                mRecyclerView.stopScroll();
            }

        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            //Log.d(TAG, "SCROLL STATE CHANGE : " + String.valueOf(newState));
        }
    }


    /*
     * References to RecyclerView and Adapter to reset the list to its
     */
    private ViewAdapter mAdapter;
    private RecyclerView mRecyclerView;

    private RecyclerViewScrollListener recyclerViewScrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CheckNetworkStatusTask checkNetworkStatusTask = new CheckNetworkStatusTask();

        checkNetworkStatusTask.execute(RepositoriesActivity.this);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_repositories);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        /*
         * Use this setting to improve performance if you know that changes in content do not
         * change the child layout size in the RecyclerView
         */
        mRecyclerView.setHasFixedSize(true);

        /*
         * Inicializando o adapter com uma quantidade 'fake' de items disponiveis
         *     A intenção é fazer com que o Adatper seja solicitado a preencher Views
         *     e iniciar a logica interna para carga do DataSource
         */
        mAdapter = new ViewAdapter(SUPPOSED_INITIAL_NUMBER_OF_ITEMS);
        mRecyclerView.setAdapter(mAdapter);

        // Listener
        mAdapter.setOnItemClickListener(this);

        // Configurando o Listener para o Scrolling do Recycler View
        recyclerViewScrollListener = new RecyclerViewScrollListener();
        mRecyclerView.addOnScrollListener(recyclerViewScrollListener);
    }

    /**
     * Quando o usuario tocar em um repositorio na lista
     * @param clickedItemIndex Indice clicado
     * @param repositoryInfo  Dados relacionados ao item clicado
     */
    @Override
    public void onRepositoryItemClick(int clickedItemIndex, RepositoriesInfo.RepositoryInfo repositoryInfo) {

        // O sistema abre a Activity para exibir os PullRequests deste repositório

        Log.d(TAG, "Item Clicked[" + String.valueOf(clickedItemIndex) + "], Owner[" + repositoryInfo.owner.login + "], Repository[" + repositoryInfo.name + "]"  );

        Log.d(TAG, "Opening PullRequestsActivity for Owner[" + repositoryInfo.owner.login + "], Repository[" + repositoryInfo.name + "]"  );

        Context context = RepositoriesActivity.this;
        Class destinationActivity = PullRequestsActivity.class;

        Intent openPullRequestsActivityIntent = new Intent(context, destinationActivity);

        openPullRequestsActivityIntent.putExtra( Constants.PARAM_OWNER_LOGIN, repositoryInfo.owner.login );
        openPullRequestsActivityIntent.putExtra( Constants.PARAM_REPOSITORY_NAME, repositoryInfo.name );

        startActivity(openPullRequestsActivityIntent);
    }

    @Override
    protected void onDestroy() {

        mRecyclerView.removeOnScrollListener(this.recyclerViewScrollListener);

        super.onDestroy();
    }


    class NetworkAvailableStatus
    {
        Context context;

        public boolean network;
        public boolean internet;
    }

    /**
     * Apenas verifica o status de conexão com a rede e com a internet e exibe mensagens ao usuario em caso de rede indisponivel
     */
    public class CheckNetworkStatusTask extends AsyncTask<Context,Void,NetworkAvailableStatus> {


        @Override
        protected NetworkAvailableStatus doInBackground(Context... contexts) {

            Context context = contexts[0];

            NetworkAvailableStatus status = new NetworkAvailableStatus();

            status.context = context;

            if(NetworkUtils.isNetworkConnected(context)) {
                status.network = true;
                status.internet = NetworkUtils.isInternetAvailable();
            }

            //Toast toast = Toast.makeText(context, "Network connection is not available", Toast.LENGTH_SHORT);
            //toast.show();

            return status;
        }

        @Override
        protected void onPostExecute(NetworkAvailableStatus status) {

            if(!status.network){

                CharSequence text = "Network not available!";

                Toast toast = Toast.makeText(status.context, text, Toast.LENGTH_SHORT);
                toast.show();

            }else if(!status.internet){

                CharSequence text = "Internet not available!";

                Toast toast = Toast.makeText(status.context, text, Toast.LENGTH_SHORT);
                toast.show();
            }


            super.onPostExecute(status);
        }
    }
}
