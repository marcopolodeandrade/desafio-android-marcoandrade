package com.marcoandrade.androidchallenge.templates;

import java.util.HashMap;

/**
 * Encapulsando a forma como alguns valores serão mantidos em memória
 * Quando e se for necessário implementar limpeza de cache esta classe poderá ser extendida
 */
public final class IndexedCache<TItem> {


    public HashMap<Integer, TItem> mItems = new HashMap<Integer, TItem>();

    public int size(){
        return mItems.size();
    }

    /**
     * Seta o valor de um item
     *       Insere um novo ou atualiza se já existir o id
     */
    public void set(int id, TItem item){

        mItems.put(id,item);
    }

    /**
     * Verifica se existe um item para o id
     */
    public boolean exists(int id){
        return mItems.containsKey(id);
    }

    /**
     * Recupera o item de um id (por segurança, usar a funcao exists antes)
     */
    public TItem get(int id){
        return mItems.get(id);
    }

}
