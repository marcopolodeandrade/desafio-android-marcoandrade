package com.marcoandrade.androidchallenge.templates;

/**
 * Created by MarcoPolo on 12/9/2017.
 */

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * DataSourceSimpleCache: Responsavel por manter items em memória e disponibilizá-los de forma
 *                        transparente, adaptando a consulta por indice externo para o indice interno
 *
 *                        Ex.: É possível ter apenas 60 itens em memória (Indice interno de 0-59)
 *                        Mas externamente este indice corresponder a outro range (Indice externo 120-179)
 *
 *                        Usado em conjunto com o RecyclerView
 * */
public class DataSourceCache<TDataSourceItem>{

    private String TAG = "DataSourceCache<>";

    private int currentBaseIndex = 0;

    private List<TDataSourceItem> mDataSource = new ArrayList<>();

    private int translateToInternalIndex(int index){
        return index - currentBaseIndex;
    }

    private int translateToExternalIndex(int internalIndex){
        return internalIndex + currentBaseIndex;
    }

    public static final String messageIndexOutOfRange = "Index out of range. Use the function isAvailable() before using get() to avoid this error";

    /**
     * Retorna o menor indice disponivel para consulta neste cache
     * */
    public int getMinIndexAvailable(){
        return currentBaseIndex;
    }

    /**
     * Retorna o maior indice disponivel para consulta neste cache
     * */
    public int getMaxIndexAvailable(){
        int maxInternalIndex = mDataSource.size()-1;
        return translateToExternalIndex(maxInternalIndex);
    }

    /**
     * Converte o maior Indice disponivel para Quantidade disponivel
     * */
    public int getMaxQuantityAvailable(){
        return getMaxIndexAvailable()+1;
    }

    /**
     * Se o dataSource interno está vazio
     * */
    public boolean isEmpty(){
        return (mDataSource.size() == 0);
    }

    /**
     * Adiciona itens ao final da lista
     */
    public void add(List<TDataSourceItem> newItemsList){

        System.out.println(TAG + ": " + "Appending " + String.valueOf(newItemsList.size()) + " items...");

        for(TDataSourceItem newItem: newItemsList){
            mDataSource.add(newItem);
        }

        logCacheStatus();
    }


    /**
     * Adiciona items no inicio da lista
     */
    public boolean insertBefore(List<TDataSourceItem> newItemsList){

        boolean inserted = false;

        try{
            int newItemsCount = newItemsList.size();

            // Este cache paginado nao permite interir items que teriam o index negativo

            if(currentBaseIndex - newItemsCount >= 0)
            {

                System.out.println(TAG + ": " + "Inserting " + String.valueOf(newItemsCount) + " >>>(list)...");

                List<TDataSourceItem> newList = new ArrayList<>();

                for (TDataSourceItem newItem : newItemsList) {
                    newList.add(newItem);
                }

                for (TDataSourceItem prevItem : mDataSource) {
                    newList.add(prevItem);
                }

                mDataSource = newList;

                currentBaseIndex -= newItemsCount;

                inserted = true;
            }

            logCacheStatus();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return inserted;
    }

    /**
     * Descarta uma quantidade de items do inicio da lista (intencao: limpar memoria quando rolar a lista para baixo)
     */
    public void removeFromBegin(int quantity){

        try{

            System.out.println(TAG + ": " + "Removing " + String.valueOf(quantity) + " <<<(list)...");

            List<TDataSourceItem> newList = new ArrayList<>();

            for(int preserveIndex = quantity; preserveIndex < mDataSource.size() ; preserveIndex ++ )
            {
                newList.add(mDataSource.get(preserveIndex));
            }

            mDataSource = newList;

            if(mDataSource.size()==0) {
                // the cache was cleaned
                currentBaseIndex = 0;
            }
            else {
                currentBaseIndex += quantity;
            }

            logCacheStatus();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Descarta uma quantidade de items do final da lista (intencao: limpar memoria quando rolar a lista para cima)
     */
    public void removeFromEnd(int quantity){

        try {

            System.out.println(TAG + ": " + "Removing " + String.valueOf(quantity) + " (list)<<<...");

            List<TDataSourceItem> newList = new ArrayList<>();

            int preserveQty = mDataSource.size() - quantity;

            for (int preserveIndex = 0; preserveIndex < preserveQty; preserveIndex++) {
                newList.add(mDataSource.get(preserveIndex));
            }

            mDataSource = newList;

            logCacheStatus();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /** Verifica se existe item disponível para este Ìndice aparente
     *
     * @param index indice externo (zero-based)
     * @return true se tem item disponivel neste indice
     */
    public boolean isAvailable(int index){
        int internalIndex = translateToInternalIndex(index);
        return isAvailableInternal(internalIndex);
    }

    private boolean isAvailableInternal(int internalIndex)
    {
        return (internalIndex >= 0 && internalIndex < mDataSource.size());
    }

    /**
     * Recupera um Item do cache referente ao index (zero-based) consultado
     *   IMPORTANTE:   Executar antes a funcao isAvailable() para garantir que o indice existe no cache interno
     * @param index indice do item pesquisado
     * @return Item
     */
    public TDataSourceItem get(int index) {

        try{

            int internalIndex = translateToInternalIndex(index);

            if(!isAvailableInternal(internalIndex)) {
                // The consumer class must be use the method isAvailable(int index) before use get(int index) to make sure the item exists
                return  null;
            }

            System.out.println(TAG + ": " + "getting item. externalIndex = " + String.valueOf(index) + "; internalIndex = " + String.valueOf(internalIndex) );

            return mDataSource.get(internalIndex);
        }
        catch (Exception e){

            e.printStackTrace();

            System.out.println(TAG + ": " + "Erro ao tentar carregar um item do DataSource. Index = " + String.valueOf(index)
                    + ", currentBaseIndex = " + String.valueOf(currentBaseIndex) + ", "
                    + ", mDataSource.size() = " + String.valueOf(mDataSource.size()));

            return null;

            //throw  e;
        }

    }

    public void logCacheStatus(){

        System.out.println(TAG + ": " + "Cache Status:  ItemsCount = " + String.valueOf(mDataSource.size())
                + "; BaseIndex = " + String.valueOf(currentBaseIndex)
                + "; MaxIndex = " + String.valueOf(getMaxIndexAvailable())

        );
    }

}
