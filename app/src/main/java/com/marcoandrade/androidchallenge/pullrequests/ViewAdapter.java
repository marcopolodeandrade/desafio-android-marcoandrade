package com.marcoandrade.androidchallenge.pullrequests;

import android.content.Context;
import android.icu.text.DateFormat;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.marcoandrade.androidchallenge.R;
import com.marcoandrade.androidchallenge.templates.DataSourceCache;
import com.marcoandrade.androidchallenge.templates.IndexedCache;
import com.marcoandrade.androidchallenge.utility.BitmapUtils;
import com.marcoandrade.androidchallenge.utility.JsonParser;
import com.marcoandrade.androidchallenge.utility.NetworkUtils;

import com.marcoandrade.androidchallenge.pullrequests.models.PullRequestsListInfo;
import com.marcoandrade.androidchallenge.pullrequests.models.UserInfo;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * The adapter for PullRequests RecyclerView
 */
public class ViewAdapter extends RecyclerView.Adapter<ViewAdapter.PullRequestViewHolder> {

    private static final String TAG = ViewAdapter.class.getSimpleName();

    /**
     * Quantidade maxima de paginas de dados que serão mantidas no datasource
     * */
    private static final int MAX_NUMBER_OF_PAGES_KEPT_IN_MEMORY = 4;

    /**
     * A quantidade de itens que poderá aparecer na parte inferior da lista antes do DataSource carregar as nova pagina de items
     * */
    private static final int EXTRA_ITEMS_AVAILABLE_WHILE_LOADING_ASYNC = 2;

    /**
     * Intervalo (ms) entre cada request extra feito para obter dados complementares dos usuarios
     *
     * For unauthenticated requests, the rate limit allows you to make up to 10 requests per minute.
     */
    private static final int MIN_INTERVAL_TO_EXTRA_REQUESTS = 500;

    // Usado nos testes - False desabilita os requests do Nome e Avatar do Owner (economizando requests)
    private static final boolean ENABLE_SUB_REQUESTS = true;

    /**
     * The interface that receives onClick messages
     */
    public interface IOnItemClickListener {
        /**
         * Ocorre quando o usuario 'toca' em um pullrequest
         * @param clickedItemIndex Indice clicado
         * @param itemInfo  Dados relacionados ao item clicado
         */
        void onPulRequestItemClick(int clickedItemIndex, PullRequestsListInfo.PullRequestInfo itemInfo);
    }

    public interface IAdapterEventsListener{

        /**
         * Notifica que o dataSource está vazio
         * */
        void onEmptyDataSource();
    }


    /*
     * The number of ViewHolders that have been created.
     */
    private static int viewHolderCount;

    protected IAdapterEventsListener mAdapterEventsListener = null;

    protected IOnItemClickListener mOnItemClickListener = null;

    /**
     * Quantidade de itens disponíveis para o RecyclerView
     *     Varia de acordo com a quantidade de itens no DataSource
     *     Usado também para limitar o scrolling ascendente do recyclerview
     * */
    private int mNumberOfItemsAvaiable;

    // A posição solicitada anteriormente no onBindViewHolder
    private int mPreviousPositionRequired = 0;

    // The Owner/Repository that this Adapter is working with
    private String mOwnerName;
    private String mRepositoryName;

    // Items disponíveis para a View
    DataSourceCache<PullRequestsListInfo.PullRequestInfo> mDataSourceCache = new DataSourceCache<>();

    // Informmações de usuarios mantidas em memória (passou a ser static para economizar requests em ambiente de testes)
    static IndexedCache<UserInfo> userInfoCache = new IndexedCache<UserInfo>();

    // Itens de visualização que aguardam o loading assíncrono para serem preenchidos
    List<ViewAdapter.PendingBind> pendingBindList = new ArrayList<>();

    // TODO: Melhoria: A lista abaixo é na prática utilizada como Fila,  portanto, trocar por uma implementação de fila
    // Lista de Owners que precisam ser exibidos na view...
    ArrayList<PendingGetUserInfo> pendingGetUserInfoList = new ArrayList<>();

    // Paginacao no adapter para consultar a fonte de dados

    // Ultima pagina carregada para o DataSource
    int lastPageNumberLoaded = 0;

    // Valores da 'primeira' pagina carregada no datasource (numero e quantidade de itens)
    int currentDataSourceFirstPageNumber = 0;
    int currentDataSourceFirstPageCount = 0;

    // Valores da 'ultima' pagina carregada no datasource (numero e quantidade de itens)
    int currentDataSourceLastPageNumber = 0;
    int currentDataSourceLastPageCount = 0;


    /** true indica que o RecyclerView está solicitando items de posicao anterior (usuario rolando a lista para o topo)*/
    boolean isScrollingToBegin = false;


    // Sinaliza que uma consulta está em andamento , carregando um novo bloco de dados para o mDataSourceFake
    // A variavel é atualizada somente pela Thread da UI
    boolean isLoadingAsync = false;

    public boolean isLoadingDataAsync()
    {
        return isLoadingAsync;
    }

    /**
     * Constructor
     * @param numberOfItems Numero de itens inicial (aparente)
     *                      - O valor permite que o RecyclerView solicite ao Adapter no maximo esta quantidade inicial de itens,
     *                      o que por sua vez dispara o load async da primeira pagina de daodos
     *                      (O valor precisa ser maior que zero)
     * @param ownerName login do owner
     * @param repositoryName nome do pullrequest consultado
     */
    public ViewAdapter(int numberOfItems, String ownerName, String repositoryName) {
        mNumberOfItemsAvaiable = numberOfItems;
        viewHolderCount = 0;

        mOwnerName = ownerName;
        mRepositoryName = repositoryName;
    }

    public void setAdapterEventsListener(IAdapterEventsListener listener){
        this.mAdapterEventsListener = listener;
    }

    public void setOnItemClickListener(IOnItemClickListener listener){
        this.mOnItemClickListener = listener;
    }

    /**
     * This method simply returns the number of items to display. It is used behind the scenes
     * to help layout our Views and for animations.
     *
     * @return The number of items available in our forecast
     */
    @Override
    public int getItemCount() {
        return mNumberOfItemsAvaiable;
    }

    /** Quando executada, verifica as últimas paginas consultadas e elimina do cache itens desnecessarios para a consulta atual do RecyclerView
     *  Utilizado para retirar da memória dados que 'ficaram pra trás' no scrolling, e que talvez não sejam mais utilizados */
    private void cleaningCache()
    {
        if(currentDataSourceLastPageNumber - lastPageNumberLoaded >= MAX_NUMBER_OF_PAGES_KEPT_IN_MEMORY)
        {
            // Remove a ultima 'pagina' do cache
            mDataSourceCache.removeFromEnd(currentDataSourceLastPageCount);

            // Atualiza o contador..
            currentDataSourceLastPageNumber -= 1;
        }
        else if(lastPageNumberLoaded - currentDataSourceFirstPageNumber >= MAX_NUMBER_OF_PAGES_KEPT_IN_MEMORY)
        {
            // Remove o primeiro 'bloco' do cache
            mDataSourceCache.removeFromBegin(currentDataSourceFirstPageCount);

            // Atualiza o contador
            currentDataSourceFirstPageNumber += 1;
        }
    }

    /**
     * This gets called when each new ViewHolder is created. This happens when the RecyclerView
     * is laid out. Enough ViewHolders will be created to fill the screen and allow for scrolling.
     *
     * @param viewGroup The ViewGroup that these ViewHolders are contained within.
     * @param viewType  If your RecyclerView has more than one type of item (which ours doesn't) you
     *                  can use this viewType integer to provide a different layout. See
     *                  {@link android.support.v7.widget.RecyclerView.Adapter#getItemViewType(int)}
     *                  for more details.
     * @return A new NumberViewHolder that holds the View for each list item
     */
    @Override
    public PullRequestViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.pullrequest_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        PullRequestViewHolder viewHolder = new PullRequestViewHolder(view);

        viewHolder.clearLoadingMessage();
        viewHolder.empty();

        viewHolderCount++;

        Log.d(TAG, "onCreateViewHolder: ViewHolders created (" + String.valueOf(viewHolderCount) + ")");

        return viewHolder;
    }

    /** true indica que:
     *      o Adapter sugere que o scroll seja interrompido pois os dados solicitados ainda estão sendo carregados
     */
    public boolean shouldHoldOnTheScroll(){

        // REGRA: Para sugerir que o scroll seja interrompido
        //     Se o usuario está subindo a lista e já temos mais de 2 Views aguardando pelo bind
        return (isScrollingToBegin && pendingBindList.size()>=EXTRA_ITEMS_AVAILABLE_WHILE_LOADING_ASYNC);
    }

    /**
     * OnBindViewHolder is called by the RecyclerView to display the data at the specified
     * position. In this method, we update the contents of the ViewHolder to display the correct
     * indices in the list for this particular position, using the "position" argument that is conveniently
     * passed into us.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(PullRequestViewHolder holder, int position) {

        Log.d(TAG, "onBindViewHolder: " + "# " + position);

        // Se deseja carregar um item indisponivel no cache
        if(!mDataSourceCache.isAvailable(position)){

            // Se pela 'position' solicitada  concluí-se que o RecyclerView está voltando para o topo...
            isScrollingToBegin = (position < mPreviousPositionRequired);

            Log.d("onBindViewHolder", "Data Unavailable: Position " + String.valueOf(position) + ". Saving ViewHolder for later fill");

            if (pendingBindList.size() == 0) {
                holder.showLoadingMessage(); // O primeiro item aguardando a carga exibe o 'loading...'
            }

            holder.empty(); // apaga os dados da entidade

            // Guarda a referencia do viewHolder para preenchimento posterior
            pendingBindList.add(new PendingBind(holder, position));

            if(!isLoadingAsync) {
                isLoadingAsync = true;

                // Calcula qual o numero da pagina que está sendo necessario nesta consulta
                int pageNumberNeeded = mDataSourceCache.isEmpty() ? 1 :
                        (position> mDataSourceCache.getMaxIndexAvailable()) ? currentDataSourceLastPageNumber+1 : currentDataSourceFirstPageNumber-1;

                Log.d("onBindViewHolder", "Requesting Page (for GitHub):  " + String.valueOf(pageNumberNeeded));
                getDataFromGitHubAsync(mOwnerName, mRepositoryName,pageNumberNeeded);
            }

        }
        else{

            PullRequestsListInfo.PullRequestInfo itemInfo = mDataSourceCache.get(position);

            // O item esta disponivel no dataSource. Basta exibir
            holder.clearLoadingMessage();
            holder.bind(itemInfo);

            // Se já possui os dados complementares do usuario/autor.. basta exibir
            if(userInfoCache.exists(itemInfo.author.id)) {
                UserInfo userInfo = userInfoCache.get(itemInfo.author.id);
                holder.bind(userInfo);
            }
            else {
                // Incluir o viewHolder na lista para receber os dados complementares do usuario
                pendingGetUserInfoList.add(new PendingGetUserInfo(holder));
            }

            checkNextPendingUserInfo();
        }

        // Guarda a ultima posicao solicitada
        mPreviousPositionRequired = position;

    }



    class PullRequestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        // References to last DataObjects used in bind operation for this ViewHolder
        PullRequestsListInfo.PullRequestInfo mDataItemInfo = null;

        TextView loadingTextView;

        TextView prTitleView;
        TextView prBodyView;
        TextView prUserNameView;
        TextView prUserFullNameView;
        TextView prDate;

        View prFrameDate;

        ImageView prUserAvatarView;


        public PullRequestViewHolder(View itemView) {
            super(itemView);

            // Getting view objects
            loadingTextView = (TextView) itemView.findViewById(R.id.pr_loading_text);

            prTitleView = (TextView) itemView.findViewById(R.id.pr_title);
            prBodyView = (TextView) itemView.findViewById(R.id.pr_body);
            prUserNameView = (TextView) itemView.findViewById(R.id.pr_user_name);
            prUserFullNameView = (TextView) itemView.findViewById(R.id.pr_user_full_name);
            prDate = (TextView) itemView.findViewById(R.id.pr_date);
            prFrameDate = (View) itemView.findViewById(R.id.pr_frame_date);

            prUserAvatarView = (ImageView) itemView.findViewById(R.id.pr_user_avatar);

            itemView.setOnClickListener(this);
        }


        void showLoadingMessage() {
            loadingTextView.setText("Loading...");
        }

        void clearLoadingMessage(){
            loadingTextView.setText("");
        }


        void bind(PullRequestsListInfo.PullRequestInfo itemData) {

            // Saving the data object reference used, for future operations
            this.mDataItemInfo = itemData;

            prTitleView.setText(itemData.title);
            prBodyView.setText(itemData.body);
            prUserNameView.setText(itemData.author.login);

            prUserFullNameView.setText("");

            prUserAvatarView.setImageResource(R.mipmap.ic_launcher);
            prUserAvatarView.setVisibility(View.VISIBLE);

            String createdAtValue = "";

            if(Build.VERSION.SDK_INT >= 24) {
                // TODO: Analisar o ajuste feito para API 24+ : Formato de data e hora
                // Formata de acordo com as configurações regionais do usuario
                createdAtValue = DateFormat.getDateTimeInstance().format(itemData.createdAt);
            }
            else {
                // Medida paleativa: exibindo a data em formato universal  para API abaixo de 24
                createdAtValue = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(itemData.createdAt);
            }

            prDate.setText(createdAtValue);

            prFrameDate.setVisibility(View.VISIBLE);
        }

        void bind(UserInfo UserInfo){

            // Owner's Full Name
            prUserFullNameView.setText((UserInfo.name != null)? UserInfo.name: "");

            // Owner's picture (avatar)
            if(UserInfo.avatarBitmap != null) {
                prUserAvatarView.setImageBitmap(UserInfo.avatarBitmap);
                prUserAvatarView.setVisibility(View.VISIBLE);
            }
            else {
                prUserAvatarView.setImageResource(R.mipmap.ic_launcher);
                prUserAvatarView.setVisibility(View.VISIBLE);
            }
        }

        void empty() {

            // forgets the data object used once
            this.mDataItemInfo = null;

            prUserAvatarView.setVisibility(View.INVISIBLE);
            prFrameDate.setVisibility(View.INVISIBLE);

            prTitleView.setText("");
            prBodyView.setText("");
            prUserNameView.setText("");
            prUserFullNameView.setText("");
            prDate.setText("");
        }

        /**
         * Called whenever a user clicks on an item in the list.
         * @param v The View that was clicked
         */
        @Override
        public void onClick(View v) {

            // Se a view está sem a entidade relacionada... apenas ignora o click
            if(null != mDataItemInfo)
            {
                if (null != mOnItemClickListener) {
                    int clickedPosition = getAdapterPosition();

                    // O evento informa a posicao do item e os dados do item relacionado
                    mOnItemClickListener.onPulRequestItemClick(clickedPosition, mDataItemInfo);
                }
            }
        }
    }

    class PendingBind{

        public int position;
        public PullRequestViewHolder viewHolder;

        public PendingBind(PullRequestViewHolder pViewHolder, int pPosition){
            viewHolder = pViewHolder;
            position = pPosition;
        }
    }

    class PendingGetUserInfo
    {
        // Referencia para os dados do PullRequest relacionado ao ViewModel no momento em que a 'pendência' foi gerada
        public final PullRequestsListInfo.PullRequestInfo pullRequestInfo;

        // Referencia para o ViewHolder que exibirá os dados do Usuario
        public final PullRequestViewHolder viewHolder;

        // Carregado posteriormente
        public UserInfo userInfoResult;

        public PendingGetUserInfo(PullRequestViewHolder pViewHolder){

            // Guarda a referencia para os dados do pullrequest
            pullRequestInfo = pViewHolder.mDataItemInfo;

            // Guarda a referencia para a viewHolder
            this.viewHolder = pViewHolder;
        }

        // Verifica se o viewHolder, do momento que a pendencia foi criada até agora, não mudou de entidade
        //           (o viewHolder naturalmente muda de posição na View e passa a ser usado para outro pullrequest e owner)
        public boolean checkEntityDoesNotChangedToViewHolder()
        {
            return (viewHolder.mDataItemInfo != null && pullRequestInfo.id == viewHolder.mDataItemInfo.id);
        }
    }

    void bindPendingViewHolderList()
    {
        Log.d(TAG, "Binding pending views ... (" + pendingBindList.size() + ") waiting");

        for (ViewAdapter.PendingBind pendingBind: pendingBindList){

            PullRequestViewHolder viewHolder = pendingBind.viewHolder;

            viewHolder.clearLoadingMessage();

            if( mDataSourceCache.isAvailable(pendingBind.position))
            {
                PullRequestsListInfo.PullRequestInfo itemInfo = mDataSourceCache.get(pendingBind.position);
                viewHolder.bind(itemInfo);

                // Se já possui os dados complementares do owner.. .
                if(userInfoCache.exists(itemInfo.author.id)){
                    UserInfo userInfo = userInfoCache.get(itemInfo.author.id);
                    viewHolder.bind(userInfo); // exibe
                }
                else {
                    // Coloca este view holder para receber o restante dos dados do owner
                    pendingGetUserInfoList.add(new PendingGetUserInfo(viewHolder));
                }
            }
            else{
                // Com um efetivo controle do Scroll, esta exceção não deve ocorrer
                String message ="***** bindPendingViewHolderList : pendingBind.position (" + String.valueOf(pendingBind.position) + ") indisponivel no mDataSourceCache";
                Log.d(TAG,  message);

                // Deixa em branco
                viewHolder.empty();
            }
        }

        // Ao encerrar, limpa a lista de Bindings pendentes
        pendingBindList.clear();

        checkNextPendingUserInfo();
    }

    void processResult(int pageNumber, PullRequestsListInfo result)
    {
        if(result.items.size()>0) {

            Log.d(TAG, "processResult: " + result.items.size() + " items was received from GitHub");

            if (currentDataSourceFirstPageNumber == 0) { // First Load

                currentDataSourceFirstPageNumber = pageNumber;
                currentDataSourceFirstPageCount = result.items.size();

                currentDataSourceLastPageNumber = pageNumber;
                currentDataSourceLastPageCount = result.items.size();

                // Adiciona os items no cache
                mDataSourceCache.add(result.items);
            } else if (pageNumber > currentDataSourceLastPageNumber) {
                // Se carregou uma página posterior a ultima carregada

                // Guarda o numero da pagina e a quantidade carregada
                currentDataSourceLastPageNumber = pageNumber;
                currentDataSourceLastPageCount = result.items.size();

                // Adiciona os items ao final do Cache
                mDataSourceCache.add(result.items);
            } else if (pageNumber < currentDataSourceFirstPageNumber) {
                // Se carregou uma pagina anterior a primeira que esta em cache

                currentDataSourceFirstPageNumber = pageNumber;
                currentDataSourceFirstPageCount = result.items.size();

                // Adiciona os items no inicio da lista em cache
                if(!mDataSourceCache.insertBefore(result.items)){
                    Log.d(TAG, "mDataSourceCache.insertBefore() nao pode inserir items abaixo do index 0 (zero). Aprimore os testes unitarios");
                }
            }

            // Quarda o numero da ultima pagina carregada
            lastPageNumberLoaded = pageNumber;
        }
        else
        {
            Log.d(TAG, "processResult: EMPTY RESULT was received from GitHub");

            if(mDataSourceCache.isEmpty()) {
                if (null != mAdapterEventsListener)
                    mAdapterEventsListener.onEmptyDataSource();
            }
        }

        // Preenche views que aguardavam este loading (se existirem na lista de espera)...
        bindPendingViewHolderList();

        // Retira da memória itens desnecessários para a posição atual da visualização
        cleaningCache();

        // Atualiza a quantidade de items (aparente) disponiveis para o RecyclerView
        //             Obs.: O Adapter informa a quantidade aparente para limitar quantos items o RecyclerView
        //                   na parte inferior da lista , enquanto os dados da nova pagina ainda são carregados
        mNumberOfItemsAvaiable = mDataSourceCache.getMaxQuantityAvailable()+ EXTRA_ITEMS_AVAILABLE_WHILE_LOADING_ASYNC;
    }

    /**
     * Inicializa a Task que carrega a consulta do git hub
     */
    private void getDataFromGitHubAsync(String ownerName, String repositoryName, int pageNumber) {

        URL githubSearchUrl = NetworkUtils.buildUrlGetGitHubRepositoryPullRequests(ownerName, repositoryName, pageNumber);

        LoadPullRequestsPageTask queryTask = new LoadPullRequestsPageTask(pageNumber);

        queryTask.execute(githubSearchUrl);
    }

    /** Used only for the parser function */
    static class PullRequestsInfoArray extends ArrayList<PullRequestsListInfo.PullRequestInfo>{}

    class LoadPullRequestsPageTask extends AsyncTask<URL, Void, String> {

        private final String TAG = LoadPullRequestsPageTask.class.getSimpleName();

        int mPageNumber;

        public LoadPullRequestsPageTask(int pageNumber){
            mPageNumber = pageNumber;
        }

        @Override
        protected void onPreExecute() {
            // This runs on UI Thread so .. we can update some view here
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(URL... urls) {

            String searchResults = null;

            try {

                URL searchUrl = urls[0];

                searchResults = NetworkUtils.getResponseFromHttpUrl(searchUrl);

            } catch (IOException e) {
                Log.d(TAG, "Falha ao obter o Response do gitHub. Erro abaixo");
                e.printStackTrace();
            }

            return searchResults;
        }



        @Override
        protected void onPostExecute(String s) {

            // Executed by UI Thread

            //super.onPostExecute(s);
            try {

                if (s != null) {

                    // Carrega o resultado da consulta para o DataSource em cache
                    PullRequestsListInfo result = new PullRequestsListInfo();
                    result.items = (new JsonParser<>(PullRequestsInfoArray.class)).Parse(s);

                    processResult(this.mPageNumber, result);
                }
                else{
                    Log.d(TAG, "***** onPostExecute(String s) recebeu parametro nullo");
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }

            isLoadingAsync = false;
        }

    }

    LoadUserInfoDelayedTask loadUserInfoTask = null;

    Handler startLoadUserInfoTaskHandler = new Handler();

    PendingGetUserInfo findNextPendingUserInfoValid()
    {
        PendingGetUserInfo found = null;

        while(null == found && pendingGetUserInfoList.size()>0) {

            // Recupera e remove o primeiro item da lista
            PendingGetUserInfo pendingItem = pendingGetUserInfoList.get(0);
            pendingGetUserInfoList.remove(0);

            // Se a view ainda está para o  mesmo pullrequest
            if(pendingItem.checkEntityDoesNotChangedToViewHolder()) {
                found = pendingItem;
            }

        }

        return found;

    }

    /**
     * Verifica a próximo item pendente de request
     *   Aplica as informações do cache se existir
     *   Inicializa a Task para carregar os dados do GitHub se precisar
     */
    private void checkNextPendingUserInfo() {

        // Se já tem task em andamento , apenas ignora
        if(null == loadUserInfoTask)
        {
            // Pega o proximo item pendente valido
            PendingGetUserInfo pendingGetUserInfo = findNextPendingUserInfoValid();

            if(null != pendingGetUserInfo) {

                // Obtem o Id do autor do PullRequest
                int userId = pendingGetUserInfo.pullRequestInfo.author.id;

                // Se as informações deste usuario ja estao no cache
                if(userInfoCache.exists(userId)){
                    // Basta exibir
                    UserInfo userInfo = userInfoCache.get(userId);
                    pendingGetUserInfo.viewHolder.bind(userInfo);
                }
                else {

                    loadUserInfoTask = new LoadUserInfoDelayedTask(pendingGetUserInfo);

                    startLoadUserInfoTaskHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadUserInfoTask.execute();
                        }
                    }, MIN_INTERVAL_TO_EXTRA_REQUESTS);

                }
            }
        }
    }


    /**
     * AsyncTask responsável por consultar DADOS de Owners que precisam ser exibidos (mas nao constam na consulta principal)
     */
    class LoadUserInfoDelayedTask extends AsyncTask<Void, Void, PendingGetUserInfo> {

        private final String TAG = LoadUserInfoDelayedTask.class.getSimpleName();

        PendingGetUserInfo mPendingGetUserInfo;

        public LoadUserInfoDelayedTask(PendingGetUserInfo pendingGetUserInfo){
            mPendingGetUserInfo = pendingGetUserInfo;
        }

        @Override
        protected void onPreExecute() {
            // This runs on UI Thread so .. we can update some view here
            super.onPreExecute();
        }

        @Override
        protected PendingGetUserInfo doInBackground(Void... params) {

            PendingGetUserInfo pendingGet = mPendingGetUserInfo;

            try {

                if(pendingGet.checkEntityDoesNotChangedToViewHolder())
                {
                    try {

                        if(ENABLE_SUB_REQUESTS) {
                            // Obtem os dados do Usuario (autor) para obter o 'name' (nome completo,  diferente do 'login')
                            URL getUserInfoUrl = new URL(pendingGet.pullRequestInfo.author.url);
                            String resultDataString = NetworkUtils.getResponseFromHttpUrl(getUserInfoUrl);

                            // Instancia o userInfo e preenche com o nome
                            pendingGet.userInfoResult = (new JsonParser<>(UserInfo.class)).Parse(resultDataString);

                            // Obtem o Avatar do usuario
                            URL getAvatarUrl = new URL(pendingGet.pullRequestInfo.author.avatarUrl);
                            byte[] avatarByteArray = NetworkUtils.getBytesContentFromResponseHttp(getAvatarUrl);
                            pendingGet.userInfoResult.avatarBitmap = BitmapUtils.getBitmapFrom(avatarByteArray);
                        }

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                }

            } catch (IOException e) {

                Log.d(TAG, "Falha ao obter dos dados do GitHub a respeito do Owner (name e avatar)");

                e.printStackTrace();
            }

            return pendingGet;
        }

        @Override
        protected void onPostExecute(PendingGetUserInfo result) {

            // Executed by UI Thread

            //super.onPostExecute(s);
            try {
                processDelayedResultUserInfo(result);
            }
            catch (Exception e){
                e.printStackTrace();
            }


        }
    }



    void processDelayedResultUserInfo(PendingGetUserInfo result){

        try {
            if (null != result && null != result.userInfoResult && result.checkEntityDoesNotChangedToViewHolder()) {

                UserInfo userInfo = result.userInfoResult;

                // Guarda em cache
                userInfoCache.set(userInfo.id, userInfo);

                // Exibe na view estes dados do usuário
                result.viewHolder.bind(userInfo);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally {

            // A referencia para a Task é anulada... para que uma nova task deste tipo possa ser criada..
            loadUserInfoTask = null;
        }

        // Verifica se tem mais viewHolders precisando complementar os dados do owner
        checkNextPendingUserInfo();
    }
}
