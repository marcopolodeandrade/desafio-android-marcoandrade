package com.marcoandrade.androidchallenge.pullrequests.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

/**
 * Resultado da consulta aos Pull Requests de um repositório do GitHub
 * Contem:
 *      Lista de pull requests com as principais informações do autor
 */
public class PullRequestsListInfo {

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PullRequestInfo {

        /**
         * Dados do autor carregados junto ao pull request
         */
        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class AuthorInfo{

            @JsonProperty("id")
            public int id;

            @JsonProperty("login")
            public String login;

            @JsonProperty("avatar_url")
            public String avatarUrl;

            @JsonProperty("url")
            public String url;

            public AuthorInfo(){/*used by parser*/}
        }

        @JsonProperty("id")
        public String id;

        @JsonProperty("created_at")
        public Date createdAt;

        //@JsonProperty("updated_at")
        //public Date updatedAt;

        @JsonProperty("html_url")
        public String htmlUrl;

        @JsonProperty("title")
        public String title;

        @JsonProperty("body")
        public String body;

        //@JsonProperty("state")
        //public String state;

        @JsonProperty("user")
        public AuthorInfo author;

        public PullRequestInfo(){/*used by parser*/}
    }

    /**
     * Pull Requests
     */
    public List<PullRequestInfo> items;


}
