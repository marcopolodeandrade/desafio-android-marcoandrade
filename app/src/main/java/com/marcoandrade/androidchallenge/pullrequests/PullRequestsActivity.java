package com.marcoandrade.androidchallenge.pullrequests;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.marcoandrade.androidchallenge.Constants;
import com.marcoandrade.androidchallenge.R;
import com.marcoandrade.androidchallenge.pullrequests.models.PullRequestsListInfo;

public class PullRequestsActivity extends AppCompatActivity
        implements  ViewAdapter.IOnItemClickListener,
                    ViewAdapter.IAdapterEventsListener
{
    private static final String TAG = PullRequestsActivity.class.getSimpleName();
    private static final int SUPPOSED_INITIAL_NUMBER_OF_ITEMS = 5;

    class RecyclerViewScrollListener extends RecyclerView.OnScrollListener
    {
        final String TAG = RecyclerViewScrollListener.class.getSimpleName();

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            super.onScrolled(recyclerView, dx, dy);


            if(mAdapter.shouldHoldOnTheScroll()){

                // Uma nova pagina de itens está sendo carregada em background
                // E o scrolling precisa ser interrompido até a carga completa destes dados

                Log.d(TAG, "Stopping the scroll: Waiting for dataSource be updated...");
                mRecyclerView.stopScroll();
            }

        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            //Log.d(TAG, "SCROLL STATE CHANGE : " + String.valueOf(newState));
        }
    }

    /** For which Owner this activity was opened */
    String mOwnerName;

    /** For which repository this activity was opened */
    String mRepositoryName;

    /*
     * References to RecyclerView and Adapter to reset the list to its
     */
    private ViewAdapter mAdapter;
    private RecyclerView mRecyclerView;

    private RecyclerViewScrollListener recyclerViewScrollListener;

    /**
     * Obtem os valores Extras , do Intent, necessários para esta Activity
     * @param intentWhichStartedThis
     * @return true se os dados foram encontrados e recuperados
     */
    private boolean getExtraDataExpected(Intent intentWhichStartedThis){

        if(   intentWhichStartedThis.hasExtra(Constants.PARAM_REPOSITORY_NAME)
                && intentWhichStartedThis.hasExtra(Constants.PARAM_OWNER_LOGIN)) {

            mRepositoryName = intentWhichStartedThis.getStringExtra(Constants.PARAM_REPOSITORY_NAME);
            mOwnerName = intentWhichStartedThis.getStringExtra(Constants.PARAM_OWNER_LOGIN);

            return (mRepositoryName != "" && mOwnerName != "");
        }
        else {
            return false;
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);

        Intent intentWhichStartedThis = getIntent();

        if(getExtraDataExpected(intentWhichStartedThis)){

            mRecyclerView = (RecyclerView) findViewById(R.id.rv_pullrequests);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(layoutManager);

            /*
             * Use this setting to improve performance if you know that changes in content do not
             * change the child layout size in the RecyclerView
             */
            mRecyclerView.setHasFixedSize(true);

            /*
             * Inicializando o adapter com uma quantidade 'fake' de items disponiveis
             *     A intenção é fazer com que o Adatper seja solicitado a preencher Views
             *     e iniciar a logica interna para carga do DataSource
             */
            mAdapter = new ViewAdapter(SUPPOSED_INITIAL_NUMBER_OF_ITEMS, mOwnerName, mRepositoryName);
            mRecyclerView.setAdapter(mAdapter);

            // Listener
            mAdapter.setOnItemClickListener(this);
            mAdapter.setAdapterEventsListener(this);

            // Configurando o Listener para o Scrolling do Recycler View
            recyclerViewScrollListener = new RecyclerViewScrollListener();
            mRecyclerView.addOnScrollListener(recyclerViewScrollListener);

        }
        else{
            // TODO: Fechando arestas: Tratar quando a activity não obter os "extras" necessarios para a consulta
        }

    }

    // Quando a consulta ao GitHub retornar uma lista vazia de PullRequests
    @Override
    public void onEmptyDataSource(){

        View messageEmptyListView = findViewById(R.id.pr_list_empty_message);
        messageEmptyListView.setVisibility(View.VISIBLE);

        Context context = getApplicationContext();
        CharSequence text = "No pull requests found";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    /**
     * Quando o usuario tocar em um PullRequest na lista
     * @param clickedItemIndex Indice clicado
     * @param itemInfo  Dados relacionados ao item clicado
     */
    @Override
    public void onPulRequestItemClick(int clickedItemIndex, PullRequestsListInfo.PullRequestInfo itemInfo) {

        // O sistema abre a Activity para exibir os PullRequests deste repositório

        Log.d(TAG, "PullRequest Clicked[" + String.valueOf(clickedItemIndex) + "], Title[" + itemInfo.title + "]"  );

        Log.d(TAG, "Opening a view for[" + itemInfo.htmlUrl + "]"  );

        boolean webAccessError = false;

        try {
            openWebPage(itemInfo.htmlUrl);
        }
        catch (Exception e)
        {
            Log.d(TAG,"Erro ao solicitar a abertura de uma pagina web (abaixo");
            Log.d(TAG,e.getMessage());
            webAccessError = true;
        }

        if(webAccessError) {
            Context context = getApplicationContext();
            CharSequence text = "Could not access the page, sorry!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }


    public void openWebPage(String url) {

        Uri webpage = Uri.parse(url);

        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }



    @Override
    protected void onDestroy() {

        mRecyclerView.removeOnScrollListener(this.recyclerViewScrollListener);

        super.onDestroy();
    }
}
