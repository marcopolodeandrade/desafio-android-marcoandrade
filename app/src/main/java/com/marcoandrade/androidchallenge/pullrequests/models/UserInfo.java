package com.marcoandrade.androidchallenge.pullrequests.models;

import android.graphics.Bitmap;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Informações do Autor do 'Pull Request' carregadas posteriormente
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfo {

    // Id do usuario
    public int id;

    // Nome completo do usuario
    public String name;

    // avatar/picture
    public Bitmap avatarBitmap;

    public UserInfo(){/*Used by jason parser*/}
}
